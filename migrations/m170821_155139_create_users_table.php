<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170821_155139_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'username' => $this->string(),
			'password' => $this->string(),
			'department' => $this->string(),
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
