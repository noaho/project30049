<?php

use yii\db\Migration;

/**
 * Handles the creation of table `department`.
 */
class m170821_155444_create_department_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('department', [
            'id' => $this->primaryKey(),
			'departmentName' => $this->string(),
			'headOfDepartment' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('department');
    }
}
