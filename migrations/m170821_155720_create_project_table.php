<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170821_155720_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
			'projectName' => $this->string(),
			'projectType' => $this->string(),
			'headOfProject' => $this->integer(),
			'status' => $this->integer(),
			'department' => $this->integer(),
			'startDate' => $this->date(),
			'proposedEndDate' => $this->date(),
			'endDate' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
