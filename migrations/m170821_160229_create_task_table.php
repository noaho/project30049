<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m170821_160229_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
			'projectNumber' => $this->integer(),
			'taskName' => $this->string(),
			'taskDescription' => $this->string(),
			'status' =>$this->integer(),
			'startDate' => $this->date(),
			'proposedEndDate' => $this->date(),
			'endDate' => $this->date(),
			'taskExecutor' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task');
    }
}
