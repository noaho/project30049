<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userTask`.
 */
class m170821_160944_create_userTask_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('userTask', [
            'userId' => $this->integer()->notNull(),
			'taskId' => $this->integer()->notNull(),
        ]);
			$this->addPrimaryKey('userTask_pk', 'userTask', ['userId', 'taskId']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('userTask');
    }
}
