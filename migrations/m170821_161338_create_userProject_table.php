<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userProject`.
 */
class m170821_161338_create_userProject_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('userProject', [
            'userId' => $this->integer()->notNull(),
			'projectId' => $this->integer()->notNull(),
        ]);
		$this->addPrimaryKey('userProject_pk', 'userProject', ['userId', 'projectId']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('userProject');
    }
}
