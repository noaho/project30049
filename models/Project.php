<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $projectName
 * @property string $projectType
 * @property integer $headOfProject
 * @property integer $status
 * @property integer $department
 * @property string $startDate
 * @property string $proposedEndDate
 * @property string $endDate
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['headOfProject', 'status', 'department'], 'integer'],
            [['startDate', 'proposedEndDate', 'endDate'], 'safe'],
            [['projectName', 'projectType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'projectName' => 'Project Name',
            'projectType' => 'Project Type',
            'headOfProject' => 'Head Of Project',
            'status' => 'Status',
            'department' => 'Department',
            'startDate' => 'Start Date',
            'proposedEndDate' => 'Proposed End Date',
            'endDate' => 'End Date',
        ];
    }
}
