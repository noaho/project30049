<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $projectNumber
 * @property string $taskName
 * @property string $taskDescription
 * @property integer $status
 * @property string $startDate
 * @property string $proposedEndDate
 * @property string $endDate
 * @property integer $taskExecutor
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['projectNumber', 'status', 'taskExecutor'], 'integer'],
            [['startDate', 'proposedEndDate', 'endDate'], 'safe'],
            [['taskName', 'taskDescription'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'projectNumber' => 'Project Number',
            'taskName' => 'Task Name',
            'taskDescription' => 'Task Description',
            'status' => 'Status',
            'startDate' => 'Start Date',
            'proposedEndDate' => 'Proposed End Date',
            'endDate' => 'End Date',
            'taskExecutor' => 'Task Executor',
        ];
    }
}
