<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userProject".
 *
 * @property integer $userId
 * @property integer $projectId
 */
class UserProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userProject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'projectId'], 'required'],
            [['userId', 'projectId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'projectId' => 'Project ID',
        ];
    }
}
