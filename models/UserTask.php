<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userTask".
 *
 * @property integer $userId
 * @property integer $taskId
 */
class UserTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userTask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'taskId'], 'required'],
            [['userId', 'taskId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'taskId' => 'Task ID',
        ];
    }
}
